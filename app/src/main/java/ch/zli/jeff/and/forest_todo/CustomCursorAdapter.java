package ch.zli.jeff.and.forest_todo;

import android.content.Context;
import android.database.Cursor;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import ch.zli.jeff.and.forest_todo.data.TaskContract;

/**
 * Created by ansc on 08.02.18.
 */

/**
 * Creates and binds ViewHolders to a RecyclerView to display data.
 */
public class CustomCursorAdapter extends RecyclerView.Adapter<CustomCursorAdapter.TaskViewHolder> {

    private static final String TAG = "CustomCursorAdapter" ;
    // Fields for the cursor
    private Cursor mCursor;
    private Context mContext;

    public CustomCursorAdapter(Context mContext) {
        this.mContext = mContext;
    }

    /*
    Called when ViewHolders are created to fill a RecyclerView.
     */
    @Override
    public TaskViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.task_layout, parent, false);
        return new TaskViewHolder(view);
    }

    /*
    Called to display data at a specified position
     */
    @Override
    public void onBindViewHolder(TaskViewHolder holder, int position) {

        // Indices for the _id, description, and priority columns
        int idIndex = mCursor.getColumnIndex(TaskContract.TaskEntry._ID);
        int descriptionIndex = mCursor.getColumnIndex(TaskContract.TaskEntry.COLUMN_DESCRIPTION);
        int priorityIndex = mCursor.getColumnIndex(TaskContract.TaskEntry.COLUMN_PRIORITY);

        mCursor.moveToPosition(position); // get to the right location in the cursor

        // Determine the values of the wanted data
        final int id = mCursor.getInt(idIndex);
        String description = mCursor.getString(descriptionIndex);
        int priority = mCursor.getInt(priorityIndex);

        //Set values
        holder.itemView.setTag(id);
        holder.taskDescriptionView.setText(description);

        // Programmatically set the text and color for the priority TextView
        String priorityString = "" + priority; // converts int to String
        holder.priorityView.setText(priorityString);

        GradientDrawable priorityCircle = (GradientDrawable) holder.priorityView.getBackground();
        // Get the appropriate background color based on the priority
        int priorityColor = getPriorityColor(priority);
        priorityCircle.setColor(priorityColor);
    }

    /*
    Selects the correct color
     */
    private int getPriorityColor (int priority) {
        int priorityColor = 0;

        switch (priority) {
            case 1: priorityColor = ContextCompat.getColor(mContext, R.color.materialGreen);
                break;
            case 2: priorityColor = ContextCompat.getColor(mContext, R.color.materialViolett);
                break;
            case 3: priorityColor = ContextCompat.getColor(mContext, R.color.materialBlue);
                break;
            default: break;
        }
        return priorityColor;
    }


    /*
    Returns the number of tasks
     */
    @Override
    public int getItemCount() {
        if (mCursor == null) {
            return 0;
        }
        return mCursor.getCount();
    }

    /*
    This method swaps the cursor with a new cursor after a re-query
     */
    public Cursor swapCursor(Cursor c) {
        // check if this cursor is the same as the old
        if (mCursor == c) {
            return null; // nothing changed
        }
        Cursor temp = mCursor;
        this.mCursor = c; // new cursor value

        // check if it's a valid cursor and if so then update it
        if (c != null) {
            this.notifyDataSetChanged();
        }
        return temp;
    }

    class TaskViewHolder extends RecyclerView.ViewHolder {

        TextView taskDescriptionView;
        TextView priorityView;


        public TaskViewHolder(View itemView) {
            super(itemView);

            taskDescriptionView = (TextView)itemView.findViewById(R.id.taskDescription);
            priorityView = (TextView)itemView.findViewById(R.id.priorityTextView);
        }
    }
}
