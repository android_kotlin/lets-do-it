package ch.zli.jeff.and.forest_todo;

import android.content.ContentValues;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import ch.zli.jeff.and.forest_todo.data.TaskContract;

public class AddTaskActivity extends AppCompatActivity {

    // members
    private int mPriority;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_task);

        // Set highest Priority as default (mPriority = 1)
        ((RadioButton)findViewById(R.id.radButton1)).setChecked(true);
        mPriority = 1;
    }

    /*
    This method is executed when  the "ADD" button is clicked. It get the
    user input and inserts it into the DB.
     */
    public void onClickAddTask(View view) {

        // Check if the EditText is empty, if not retrieve input and store it in an Object
        // NO entry is needed if input is empty
        String input = ((EditText)findViewById(R.id.editTextTaskDescription)).getText().toString();
        if (input.length() == 0) {
            return;
        }

        // Insert new task data via a ContentResolver
        // Create new empty ContentValues object
        ContentValues contentValues = new ContentValues();
        // put the retrieved data (Description, Priority) into the contentValues
        contentValues.put(TaskContract.TaskEntry.COLUMN_DESCRIPTION, input);
        contentValues.put(TaskContract.TaskEntry.COLUMN_PRIORITY, mPriority);
        // Insert the content values with a contentResolver
        Uri uri = getContentResolver().insert(TaskContract.TaskEntry.CONTENT_URI, contentValues);

        // Display the returned URI with a Toast
        if(uri != null) {
            Toast.makeText(getBaseContext(), uri.toString(), Toast.LENGTH_LONG).show();
        }

        // IMPORTANT! when the activity is finished return to to the MainActivity
        finish();
    }

    /*
    Is called whenever a priority button is clicked and changes the value of mPriority
     */
    public void onPrioritySelected(View view) {
        if (((RadioButton)findViewById(R.id.radButton1)).isChecked()) {
            mPriority = 1;
        } else if (((RadioButton)findViewById(R.id.radButton2)).isChecked()) {
            mPriority = 2;
        } else if (((RadioButton)findViewById(R.id.radButton3)).isChecked()) {
            mPriority = 3;
        }
    }

}
