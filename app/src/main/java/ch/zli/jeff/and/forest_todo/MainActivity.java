package ch.zli.jeff.and.forest_todo;


import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;

import ch.zli.jeff.and.forest_todo.data.TaskContract;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {


    // Constants for logging and referencing to a unique loader
    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int TASK_LOADER_ID = 0;
    RecyclerView mRecyclerView;
    // Member variables
    private CustomCursorAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Set the RecyclerView to its corresponding view
        mRecyclerView = findViewById(R.id.recyclerViewTasks);

        // Set the layout to a linear layout, which positions
        // the items in the RecyclerView to a linear list
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        // Init adapter and attach to recyclerview
        mAdapter = new CustomCursorAdapter(this);
        mRecyclerView.setAdapter(mAdapter);

        /*
        Add a touch helper to the RecyclerView to recognize swipes when the user wants
        to delete a task. This function uses a callback to signalise such actions.
         */
        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            // this method gets called when the user swipes left or right
            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                // use of getTag() to get the id of the swiped item
                int id = (int) viewHolder.itemView.getTag();

                // Build correct URI with the id appended
                String stringId = Integer.toString(id);
                Uri uri = TaskContract.TaskEntry.CONTENT_URI;
                uri = uri.buildUpon().appendEncodedPath(stringId).build();

                // Delete a single row of data
                getContentResolver().delete(uri, null, null);

                // Restart the loader to re-query all tasks after deletion
                getSupportLoaderManager().restartLoader(TASK_LOADER_ID, null, MainActivity.this);
            }
        }).attachToRecyclerView(mRecyclerView);

        /*
            Assign the Floating Action Button (FAB) to its view and
            attach an OnClickListener to it. When the button is clicked a
            new intent will be created and the AddTaskActivity will be launched
         */
        FloatingActionButton fab = (FloatingActionButton)findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Create the new intent to start the AddTaskActivity
                Intent addTaskIntent = new Intent(MainActivity.this, AddTaskActivity.class);
                startActivity(addTaskIntent);
            }
        });

        /*
         Check that a loader is initialized and active, otherwise create a new one or
         if one exists use it
         */
        getSupportLoaderManager().initLoader(TASK_LOADER_ID, null, this);
    }

    /*
    This method usually gets called when this activity was paused or restarted.
    Normally this happens after new data  has been inserted in the AddTaskActivity
    and then this method restarts the loader to re-query the data for new changes.
     */
    @Override
    protected void onResume() {
        super.onResume();

        // re-query for all tasks
        getSupportLoaderManager().restartLoader(TASK_LOADER_ID, null, this);
    }

    /**
     * This method instantiates and returns a new AsyncTaskLoader with the given Id.
     * It will return the task data as a Cursor or null if an error occurs.
     *
     * It also implements the required callbacks at all stages of loading.
     *
     */
    @Override
    public Loader<Cursor> onCreateLoader(int id, final Bundle args) {

        return new AsyncTaskLoader<Cursor>(this) {

            // Initialize a Cursor, this will hold all the task data
            Cursor mTaskData = null;

            // onStartLoading() is called when a loader first starts loading data
            @Override
            protected void onStartLoading() {
                if (mTaskData != null) {
                    // Delivers any previously loaded data immediately
                    deliverResult(mTaskData);
                } else {
                    // Force a new load
                    forceLoad();
                }
            }

            // loadInBackground() performs asynchronous loading of data
            @Override
            public Cursor loadInBackground() {
                // Will implement to load data

                // Query and load all task data in the background; sort by priority
                // [Hint] use a try/catch block to catch any errors in loading data

                try {
                    return getContentResolver().query(TaskContract.TaskEntry.CONTENT_URI,
                            null,
                            null,
                            null,
                            TaskContract.TaskEntry.COLUMN_PRIORITY);

                } catch (Exception e) {
                    Log.e(TAG, "Failed to asynchronously load data.");
                    e.printStackTrace();
                    return null;
                }
            }

            // deliverResult sends the result of the load, a Cursor, to the registered listener
            public void deliverResult(Cursor data) {
                mTaskData = data;
                super.deliverResult(data);
            }
        };
    }

    /*
    Called when the loader has finished to load.
     */
    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        // Update the adapter data which is used to create ViewHolders
        mAdapter.swapCursor(data);
    }


    /*
     Called when the previous loader is reset. It removes any references
     this activity had.
     */
    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.swapCursor(null);

    }
}
