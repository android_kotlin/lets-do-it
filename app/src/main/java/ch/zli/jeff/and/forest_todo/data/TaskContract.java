package ch.zli.jeff.and.forest_todo.data;

import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by ansc on 08.02.18.
 */

public class TaskContract {

    // which content provider to access
    public static final String AUTHORITY = "ch.zli.jeff.and.forest_todo";

    // Base content URI = "content://" + <AUTHORITY>
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + AUTHORITY);

    // path to the "tasks" directory
    public static final String PATH_TASKS = "tasks";

    /* TaskEntry is an inner class which defines the contents of the task table */
    public static final class TaskEntry implements BaseColumns {

        // TaskEntry content URI = base content URI + path
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_TASKS).build();

        // Task table and column names
        public static final String TABLE_NAME = "tasks";

        // TaskEntry implements "BaseColumns" which automatically produces "_ID" column,
        // but we need two other columns in addition
        public static final String COLUMN_DESCRIPTION = "description";
        public static final String COLUMN_PRIORITY = "priority";


    }
}
